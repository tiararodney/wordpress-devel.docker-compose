#!/bin/bash

cd $WWWDIR

if [ $( wp --allow-root config has DB_HOST 2> >(grep -ic Error) ) -eq 1 ]; then
    wp --allow-root config create --dbuser="$DBUSER" --dbname="$DBNAME" --dbhost="$( [[ "$DBHOST" == /* ]] && echo 'localhost:'; )$DBHOST" --dbpass="$DBUSERPASS" --dbprefix="$DBPREFIX" --locale="$WPLOCALE"

    wp --allow-root core install --title="$TITLE" --admin_user="$ADMINUSER" --admin_password="$ADMINUSERPASS" --url="$URL" --admin_email="$ADMINEMAIL" --skip-email
fi

rm -- "$0"
