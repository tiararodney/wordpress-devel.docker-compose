#!/bin/bash

cd "$SRCDIR"

[ $(node -p "try{require('webpack-wordpress/package.json').version}catch(e){}") != "0.0.0" ] && npm install;

echo "Starting WPS"
npm run wds --wp.rootdir="$WP__PATH" --wp.theme-id="$THEMEID" --sass.includepaths="$SASSINCLUDEPATHS"
