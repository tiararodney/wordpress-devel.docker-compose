#wordpress-devel.docker-compose
Development environment for Wordpress as a Docker composition

All container images are based of [amazonlinux Docker Hub image](https://hub.docker.com/_/amazonlinux) and are required to be built locally. Built time is expected to be ~4 minutes (on a 1-core; 1GB Linux VM).

##Get started
1. Create a `.env` file at the root of this repository. You may use the `.env.sample` as a template.
2. Execute `docker-compose build` at the root of this repository to build the environment.
3. Execute `docker-compose up` at the root of this repository to start the environment. Use the [-d or --detach flag](https://docs.docker.com/compose/reference/up/) to let it run in the background.

###Theme Development

```bash
docker-compose -f docker-compose.yml -f docker-compose.devel.yml up
```
